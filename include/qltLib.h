#ifndef QLTLIB_H
#define QLTLIB_H

#include "bebidas.h"
#include "cds.h"
#include "compras.h"
#include "doces.h"
#include "dvds.h"
#include "em_comum.h"
#include "excecao.h"
#include "frutas.h"
#include "livros.h"
#include "menu.h"
#include "perecivel.h"
#include "produtos.h"
#include "qlt.h"
#include "registro.h"
#include "salgados.h"
#include "util.h"

#endif