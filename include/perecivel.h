/**
* @file	 	perecivel.h
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Perecivel.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/


#ifndef PERECIVEL_H
#define PERECIVEL_H

namespace qltLib {
class Perecivel{

	protected:
		int validade; // validade do protudo.
		
	public:

	/** 
	 * @brief	Ḿétodo que retorna a validade do protudo.
	 * @return 	validade do produto.
	 */	
	int get_validade();


	/** 
	 * @brief	Ḿétodo que altera a validade do protudo.
	 */
	void set_validade(int val);


	/** 
	 * @brief	Ḿétodo que checa se o produto está dentro do prazo de validade.
	 * @return 	estado da validade do produto, false para estragado, true para 
	 *			bom para consumo.
	 */
	bool checa_validade(int dia);
	
};
	
}

#endif