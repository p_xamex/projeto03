/**
* @file	 	frutas.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Fruta.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/
#ifndef FRUTAS_H
#define FRUTAS_H 

#include "produtos.h"
#include "perecivel.h"

namespace qltLib {
	
class Fruta : public Produto, public Perecivel{
	private:
		int lote;
		int data;

	public:
		static int quant;
		Fruta *next;
		Fruta *prev;

		/** 
 		* @brief	Construtor padrão da classe Fruta
 		*/
		Fruta();

		/** 
 		* @brief	Destrutor padrão da classe Fruta
 		*/
		~Fruta();	

		/** 
 		* @brief	Construtor parametrizado da classe Fruta
 		* @param	c código de barras do produto.
		* @param	d descrição do produto.
		* @param	p preço do produto.
		* @param	q quantidade de unidades do produto.
		* @param	acu quantidade de acucar no doce.
		* @param	g booleana para determinar se contém gluten.
		* @param	lac booleana para determinar se contém lactose.
		* @param	val data de validade do produto.
 		*/
		Fruta(int c, string d, float p, int q, int l, int dt, int val);

		/**\defgroup Gets_e_Sets_Fruta
		* @brief 	Métodos de get e set da classe Fruta
		* @{
		*/
		void set_lote(int l);
		int get_lote();
		void set_data(int dt);
		int get_data();
		/**
		* @}
		*/

		/** 
		* @brief	Sobrecarga do operador == para para verificar se
		*			existe código de barras na lista de produtos igual a 
		*			do produto passado.
		*/
		bool operator== (Fruta &c);
};
}


#endif