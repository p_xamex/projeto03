/**
* @file	 	produtos.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Produtos.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/



#ifndef PRODUTOS_H
#define PRODUTOS_H

#include <string>
using std::string;


namespace qltLib {
class Produto {
	protected:
		int cdb;
		string desc;
		float preco;
		int quant;
		
	public:
		static int lotes;


/** 
 * @brief	Ḿétodo que altera o valor do código de barras
 */
void set_cdb(int c);


/** 
 * @brief	Ḿétodo que retorna o código de barras do produto.
 * @return 	código de barras do produto.
 */
int get_cdb();


/** 
 * @brief	Ḿétodo que altera a descrição do protudo
 */
void set_desc(string d);


/** 
 * @brief	Ḿétodo que retorna a descrição do produto.
 * @return 	descrição do produto.
 */
string get_desc();


/** 
 * @brief	Ḿétodo que altera o valor do preço.
 */
void set_preco(float p);


/** 
 * @brief	Ḿétodo que retorna o preço do produto.
 * @return 	preço do produto.
 */
float get_preco();


/** 
 * @brief	Ḿétodo que altera a quantidade de unidades do protudo.
 */
void set_quant(int q);


/** 
 * @brief	Ḿétodo que retorna a quantidade de unidades do protudo.
 * @return 	quantidade de unidades do protudo.
 */
int get_quant();

};
	
}


#endif