/**
* @file	 	doces.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Doce.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/
#ifndef  DOCES_H
#define DOCES_H

#include <iostream>
using std::cout;

#include "produtos.h"
#include "perecivel.h"

namespace qltLib {
class Doce : public Produto, public Perecivel{
	private:
		float acucar;
		bool gluten;
		bool lactose;
		
	public:
		static int quant;
		Doce *next;
		Doce *prev;

		/** 
 		* @brief	Construtor padrão da classe Doce
 		*/
		Doce();

		/** 
 		* @brief	Destrutor padrão da classe Doce
 		*/
		~Doce();

		/** 
 		* @brief	Construtor parametrizado da classe Doce
 		* @param	c código de barras do produto.
		* @param	d descrição do produto.
		* @param	p preço do produto.
		* @param	q quantidade de unidades do produto.
		* @param	acu quantidade de acucar no doce.
		* @param	g booleana para determinar se contém gluten.
		* @param	lac booleana para determinar se contém lactose.
		* @param	val data de validade do produto.
 		*/
		Doce(int c, string d, float p, int q, float acu, bool g, bool lac, int val);

		/**\defgroup Gets_e_Sets_Doce
		* @brief 	Métodos de get e set da classe Doce
		* @{
		*/
		void set_acucar(float acu);
		float get_acucar();
		void set_gluten(int g);
		bool get_gluten();
		void set_lactose(int lac);
		bool get_lactose();	
		/**
		* @}
		*/

		/** 
		 * @brief	Sobrecarga do operador == para para verificar se
		 *			existe código de barras na lista de produtos igual a 
		 *			do produto passado.
		 */
		bool operator== (Doce &c);	
	
};
}

#endif