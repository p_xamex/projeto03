/**
* @file	 	excecao.h
* @brief	Arquivo de cabeçalho contendo a definição da classe Tamanho_invalido
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/

#ifndef EXCECAO_H
#define EXCECAO_H

#include <exception>
using std::exception;

	class Tamanho_invalido : public exception
	{
		public:
			const char* what()//método herdado e alterado da classe exception.
			{
				return "Entrada inválida. Por favor, digite um valor válido.\n";;
			}
		
	};

#endif