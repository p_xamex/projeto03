/**
* @file	 	em_comum.h
* @brief	Arquivo de cabeçalho contendo as definições das funções que realizam
*			operações em comum sobre todas as listas de produtos e compra.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/

#ifndef EM_COMUM_H
#define EM_COMUM_H

#include <string>
using std::string;

#include <map>
using std::map;

#include <iostream>
using namespace std;

#include "util.h"

namespace qltLib {

/** 
* @brief	Função template que recebe um iterador de map e imprime as informações
*			de um unico produto
* @param	it iterador que aponta para o produto que sera impresso.
*/
template<typename Iterator>
void em_comum_produto(Iterator it)
{
	cout << "\nDescrição: " << it->second.get_desc() << "\nCódigo de barras: " << it->second.get_cdb();
	cout << "\nPreço: " << it->second.get_preco() << "\nQuantidade: "; 
	if(it->second.get_quant() == 0) cout << "(Produto indisponível em estoque)\n";
	else cout << it->second.get_quant() << endl;

}

/** 
* @brief	Função template que recebe um map de um tipo de produto e imprime todos os
*			seus elementos.
* @details  imprime apenas as informações que estão presentes em todos os tipos
*			de produtos.
* @param	prod map do tipo de produto a ser impresso
*/
template<typename T>
void em_comum_listar(map<int, T> prod){
	typename map<int, T>::iterator it;
	it = prod.begin();
	while(it != prod.end())
	{
	    cout << "\nDescrição: " << it->second.get_desc() << "\nCódigo de barras: " << it->second.get_cdb();
	    cout << "\nPreço: " << it->second.get_preco() << "\nQuantidade: " << it->second.get_quant() << endl;
	 	it++;   
	}
}


/** 
* @brief	Função template que consulta se um produto está na lista
*			do seu tipo respectivo de produto.
* @param	prod lista de produto de um tipo generico.
* @param	itaux iterador auxiliar que percorre o map.
* @return 	retorna cont, que indica se o produto foi encontrado ou não.
*/
template<typename T, typename Iterator>
int em_comum_consultar(map<int, T> prod, Iterator &itaux)
{
	int cont;
	cont = 0;

	cout << "\nInforme o código de barras do produto: ";
	int cdb;
	invalida(cdb);

	itaux = prod.find(cdb);
	if(itaux != prod.end())
	{	
		em_comum_produto(itaux);
		cont++;
	}

	if(cont==0) cout << "\nProduto não encontrado\n";

	return cont;
}


/** 
* @brief	Função template que remove uma quantidade específica ou todas
*			as unidades do protudo passado.
* @param	prod lista de produto de um tipo genérico.
* @param	it iterador que auxilia na remoção do produto.
* @return 	número de unidades removidas.
*/
template<typename T, typename Iterator>
int em_comum_remover(map<int, T> &prod, Iterator it)
{
	int cont;
	cont = em_comum_consultar(prod, it);

	int aux;
	aux = 0;

	if (cont > 0){

		int escolha;
		escolha = 0;

		cout <<"\n\nO que deseja fazer: ";
		cout <<"\n1 - Remover todas as unidades";
		cout <<"\n2 - Remover uma quantidade específica\n\n";

		do{
			invalida(escolha);
		}while(escolha < 1 || escolha > 2);

		if(escolha == 1){
			cout << "\nTodos as unidades de " << it->second.get_desc() << " foram removidas\n";
			prod.erase(it->first); // problema ao remover por iterador, por isso removemos por chave
			Produto::lotes--;

		}else{
			cout << "\nInforme a quantidade de unidades de " << it->second.get_desc() << " a serem removidos: ";
			int q = 0;
			do{
				invalida(q);
				if (q >= it->second.get_quant())
					cout << "\n\nValor incorreto, por favor, digite um valor de 1 a " << it->second.get_quant()-1 << endl << endl;	

			}while(q >= it->second.get_quant());

			aux = q;
			q = it->second.get_quant() - q; 
			T nprod;
			nprod = prod[it->second.get_cdb()];
			nprod.set_quant(q);
			prod[nprod.get_cdb()] = nprod;
			cout << "\nQuantidade removida\n";

			return aux;

		}
	}

	return aux;
}


/** 
* @brief	Função template que auxilia na alteração de um produto.
* @details 	instancia um objeto do tipo passado e depois o poe na map respectiva
* @param	novop produto que substituirá o produto escolhido para ser alterado
*/
template<typename T>
void em_comum_alterar(T &novop){
	
	cout << "\nDescrição: ";
	string desc;
	cin.ignore();
    getline(cin, desc, '\n');
	novop.set_desc(desc);

	cout << "\nPreço: ";
	float preco;
	invalida(preco);
	novop.set_preco(preco);

	cout << "\nQuantidade: ";
	int qnt;
	invalida(qnt);
	novop.set_quant(qnt);
}


/** 
* @brief	Função template que realiza a compra do produto passado.
* @param	itcomp produto escolhido para a compra.
* @param	prod map em que o produto escolhido para compa faz parte (usado para decrementar o produto no map).
* @param	nova_compra map que contem as compras feitas.
* @param	total valor total dos produto comprados (foi necessario fazer uso dessa variavel para alterar o membro statico Compra::total,
*			por algum motivo, acredito que erro de inclusão, ele não é alterado nessa função).
* @param	carrinho numero de produtos comprados, foi usada aqui pelo mesmo motivo da variavel total.
* @return 	retorna a quantidade comprada do produto escolhido.
*/
template<typename Iterator, typename T>
int em_comum_compra(Iterator itcomp, map<int, T> &prod, map<int, Compra> &nova_compra, float &total, int &carrinho){
	float valor = 0;
	cout << "\nComprar produto?";
	cout << "\n1 - sim";
	cout << "\n2 - não\n\n";
	int esc = 0;

	escolha_menu(esc, 1, 2);
	int qnt = 0; // quantidade que sera comprada
	if(esc == 1){
		cout << "\nQuantas unidade deseja comprar? ";
		do{
			invalida(qnt);
			if(qnt > itcomp->second.get_quant()){
 				cout << "\n\nQuantidade indiponível em estoque, por favor, digite um valor de 1 a " << itcomp->second.get_quant() << endl << endl;	
			}

		}while(qnt > itcomp->second.get_quant());

		cout << "\nConfirmar compra do produto?\nObs: Após a confimação não é possível mais cancelar a compra\n";
		cout << "\n1 - Sim\n2 - Não\n\n";
		int confirma;
		escolha_menu(confirma, 1, 2);
		if(confirma == 1)
		{
			valor = itcomp->second.get_preco() * qnt;
			total+=valor;
			carrinho+=qnt;

			//Copia os valores do produto escolhido pra um objeto Compra;
			Compra nova = nova_compra[itcomp->first];
			nova.set_desc(itcomp->second.get_desc());
			nova.set_cdb(itcomp->second.get_cdb());
			nova.set_preco(itcomp->second.get_preco());
			nova.set_quant(nova.get_quant()+qnt);
			//adiciona a nova compra ao map de compras;
			nova_compra[itcomp->first] = nova;

			//altera a quantidade em estoque do produto comprado
			qnt = itcomp->second.get_quant() - qnt;
			T altr = prod[itcomp->first];
			altr.set_quant(qnt);
			prod[itcomp->first] = altr;
		}

		return qnt;
	}

	return 0;
}


/**
* @brief Função template usada pra imprimir informações comuns entre as Classes Doce e Salgado
* @param s Obejto que tera suas informações impressas
*/
template<typename T>
void gluten_lactose_data(T s)
{
	if(s->second.get_gluten() == false){
	cout << "Não contém";
	}else{
		cout << "Contém";
	}

	cout << "\nLactose: ";
	 if(s->second.get_lactose() == false){
		cout << "Não contém";
	}else{
		cout << "Contém";
	}
	cout << "\nData de validade: " << s->second.get_validade() << endl;
}
	
}

#endif