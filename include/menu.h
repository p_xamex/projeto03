/**
* @file	 	menu.h
* @brief	Arquivo de cabeçalho contendo as definições das funções do menu,
*			responsável por gerenciar o menu de ações sobre a classe Qlt.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/


#ifndef MENU_H
#define MENU_H

#include <iostream>
#include <string>
#include <limits>
#include "util.h" 
#include "qlt.h"

namespace qltLib {
/** 
* @brief	Função resposável por receber informações do usuário e
*			chamar o metódo cadastrar correspondente passando essa informações.
* @param	prod lista principal do programa.
*/
void cadastrar(Qlt &prod);


/** 
* @brief	Função que realiza consulta em um setor ou produto específico
* @param	l lista principal do programa.
* @param 	escolha ação escolhida no menu principal.
*/
void acessar_produto(Qlt &l, int escolha);


/** 
* @brief	Função que controla as ações sobre a lista de compras
*			da classe Lista.
* @param	l lista principal do programa.
*/
void menu_compras(Qlt &l);


/** 
* @brief	Função que controla o menu principal do programa e chama
*			as funções escolhidas pelo usuário.
* @param	l lista principal do programa.
*/
void acessar_lista_p(Qlt &l);
	
}


#endif