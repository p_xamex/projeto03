/**
* @file	 	cds.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Cd.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/

#ifndef CD_H
#define CD_H

#include <string>
using std::string;

#include "produtos.h"

namespace qltLib {
class Cd : public Produto {
	private:
		string estilo;
		string artista;
		string album;

	public:
		static int quant; // contador de quantidade de produtos da classe
		Cd *next; // aponta para o próximo nó
		Cd *prev;  // aponta para o nó anterior


		/** 
		 * @brief	Construtor padrão da classe Cd
		 */		
		Cd();


		/** 
		 * @brief	Construtor parametrizado da classe Cd
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	estilo estilo do álbum.
		 * @param 	artista nome do artista.
		 * @param	album nome do álbum.
		 */
		Cd(int c, string d, float p, int q, string estilo, string artista, string album);


		/** 
		 * @brief	Destrutor padrão da classe Cd
		 */
		~Cd();


		/** 
		 * @brief	Ḿétodo que altera a estilo do cd
		 */
		void set_estilo(string &estilo);


		/** 
		 * @brief	Ḿétodo que retorna o estilo do cd
		 * @return 	estilo do cd.
		 */
		string get_estilo();


		/** 
		 * @brief	Ḿétodo que altera o artista do cd
		 */
		void set_artista(string &artista);


		/** 
		 * @brief	Ḿétodo que retorna o artista do cd
		 * @return 	artista do cd.
		 */
		string get_artista();


		/** 
		 * @brief	Ḿétodo que altera o nome do album do cd
		 */
		void set_album(string &album);


		/** 
		 * @brief	Ḿétodo que retorna o nome album do cd
		 * @return 	album do cd.
		 */
		string get_album();


		/** 
		 * @brief	Sobrecarga do operador == para para verificar se
		 *			existe código de barras na lista de produtos igual a 
		 *			do produto passado.
		 */
		bool operator== (Cd &c);
};

}

#endif