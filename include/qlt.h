/**
* @file	 	qlt.h
* @brief	Arquivo de cabeçalho contendo as definição da Classe qlt,
*			responsável por gerenciar e alterar a lista de produtos e compras.
* @details 	A classe lista contém um iterador para cada map como membro, usado para 
*			poupar linhas de código e declarações desnecessárias.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/

#ifndef QLT_H
#define QLT_H 

#include <map>
using std::map;

#include "bebidas.h"
#include "frutas.h"
#include "doces.h"
#include "salgados.h"
#include "cds.h"
#include "dvds.h"
#include "livros.h" 
#include "compras.h"
#include "em_comum.h"

namespace qltLib {

class Qlt {

	public:
		//maps para cada tipo de produto
		map<int, Bebida> beb;
		map<int, Fruta> frut;
		map<int, Doce> doce;
		map<int, Salgado> salg;
		map<int, Cd> cd;
		map<int, Dvd> dvd;
		map<int, Livro> livr;
		map<int, Compra> comp;
		//iteradores para cada map
		map<int, Bebida>::iterator itbeb;
		map<int, Fruta>::iterator itfrut;
		map<int, Doce>::iterator itdoce;
		map<int, Salgado>::iterator itsalg;
		map<int, Cd>::iterator itcd;
		map<int, Dvd>::iterator itdvd;
		map<int, Livro>::iterator itlivr;
		map<int, Compra>::iterator itcomp;


		/** 
		 * @brief	Construtor padrão da classe Qlt
		 */
		Qlt(){};


		/** 
		 * @brief	Método que lista todas os produtos cadastrados do estoque.
		 */
		void listar();


		/** 
		 * @brief	Método responsável por checar se um produto com o mesmo
		 *			código de barras já foi registrado.
		 * @param	cdb código de barras do produto.
		 */
		int checa_cdb(int cdb);


		/** 
		 * @brief	Método que consulta as informções de um determinado
		 *			produto ou setor de produtos.
		 * @param	esc escolha do usuário (setor ou produto).
		 */
		void consultar(int esc);


		/** 
		 * @brief	Método que remove um produto específico.
		 */
		void remover();


		/** 
		* @brief	Método que altera um produto específico.
		*/
		void alterar();


		/** 
		 * @brief	Método que inicia a tarefa de compra, chamando as
		 *			funções e metódos que auxiliam a atividade.
		 */
		void compra();


		/** 
		 * @brief	Método que posiciona todos os iteradores no inicio de suas
		 *			respectivas listas, necessaria quando o iterador é alterado por
		 *			outro método.
		 */
		void iterator_to_begin(); 


		/** 
		 * @brief	Método que lista todas as compras feitas.
		 */
		void lista_compras();	
};

}

#endif