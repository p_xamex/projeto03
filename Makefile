PROG = ./bin/Qlevetudo

BIN_DIR = ./bin
OBJ_DIR = ./build
DATA_DIR = ./data
LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
DOC_DIR = ./doc
TEST_DIR = ./test

CC = g++

CPPFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)/

OBJS = ./build/main.o ./build/frutas.o ./build/menu.o ./build/produtos.o ./build/perecivel.o ./build/cds.o ./build/livros.o ./build/salgados.o ./build/doces.o ./build/dvds.o ./build/util.o ./build/compras.o ./build/bebidas.o ./build/qlt.o

RM = rm -rf

.PHONY: init all doxy clean go debug

debug: CPPFLAGS += -g -O0

linux: qlevetudo.so prog_dinamico

qlevetudo.so: $(INC_DIR)/bebidas.h $(INC_DIR)/menu.h $(INC_DIR)/frutas.h $(INC_DIR)/produtos.h $(INC_DIR)/perecivel.h $(INC_DIR)/cds.h $(INC_DIR)/livros.h $(INC_DIR)/salgados.h $(INC_DIR)/doces.h $(INC_DIR)/dvds.h $(INC_DIR)/util.h $(INC_DIR)/em_comum.h $(INC_DIR)/compras.h
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/produtos.cpp -o $(OBJ_DIR)/produtos.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/perecivel.cpp -o $(OBJ_DIR)/perecivel.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/compras.cpp -o $(OBJ_DIR)/compras.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/bebidas.cpp -o $(OBJ_DIR)/bebidas.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/frutas.cpp -o $(OBJ_DIR)/frutas.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/cds.cpp -o $(OBJ_DIR)/cds.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/livros.cpp -o $(OBJ_DIR)/livros.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/salgados.cpp -o $(OBJ_DIR)/salgados.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/doces.cpp -o $(OBJ_DIR)/doces.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/dvds.cpp -o $(OBJ_DIR)/dvds.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/util.cpp -o $(OBJ_DIR)/util.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/menu.cpp -o $(OBJ_DIR)/menu.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/qlt.cpp -o $(OBJ_DIR)/qlt.o
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/qltLib.cpp -o $(OBJ_DIR)/qltLib.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/qltLib.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/produtos.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/perecivel.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/compras.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/bebidas.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/frutas.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/cds.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/livros.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/salgados.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/doces.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/dvds.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/util.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/menu.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/qlt.o

prog_dinamico:
	$(CC) $(CPPFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/qlevetudo.so -o $(BIN_DIR)/$@
	@echo "\n\n+++ MAKEFILE EXECUTADO COM SUCESSO +++\n\n"

init:
	mkdir -p $(BIN_DIR)
	mkdir -p $(OBJ_DIR)
	mkdir -p $(DATA_DIR)
	mkdir -p $(INC_DIR)
	mkdir -p $(SRC_DIR)
	mkdir -p $(DOC_DIR)
	mkdir -p $(LIB_DIR)
	mkdir -p $(TEST_DIR)

doxy:
	@rm -rf $(DOC_DIR)/*
	doxygen Doxyfile

clean:
	@rm -rf $(OBJ_DIR)/*
	@rm -rf $(DOC_DIR)/*
	@rm -rf $(BIN_DIR)/*
	@echo "\nRemovendo arquivos objeto, executaveis/binários e pasta HTML do diretório doc...\n"

go:
	$(BIN_DIR)/Qlevetudo