#include <iostream>
using namespace std;

#include <map>

int main()
{
	map<int, char> mymap;
	mymap[1] = 'a';
	map<int, char>::iterator it;

	it = mymap.begin();

	it->second = 'b';

	cout << "mmap: " << mymap[1];

	return 0;
}