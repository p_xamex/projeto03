/**
* @file	 	cds.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Cd.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#include "cds.h"

namespace qltLib {
/** 
 * @brief	Construtor padrão da classe Cd
 */
Cd::Cd() {
	estilo = "Desconhecido";
	artista = "Desconhecido";
	album = "Desconhecido";
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
}


/** 
 * @brief	Construtor parametrizado da classe Cd
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	estilo estilo do álbum.
 * @param 	artista nome do artista.
 * @param	album nome do álbum.
 */
Cd::Cd(int c, string d, float p, int q, string estilo, string artista, string album){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_estilo(estilo);
	set_artista(artista);
	set_album(album);
	next = NULL;
	prev = NULL;
	this->lotes++;
	this->quant++;
}



/** 
 * @brief	Destrutor padrão da classe Cd
 */
Cd::~Cd(){
}



/** 
 * @brief	Ḿétodo que altera a estilo do cd
 */
void Cd::set_estilo(string &estilo) {
	this->estilo = estilo;
}


/** 
 * @brief	Ḿétodo que retorna o estilo do cd
 * @return 	estilo do cd.
 */
string Cd::get_estilo() {
	return estilo;
}


/** 
 * @brief	Ḿétodo que altera o artista do cd
 */
void Cd::set_artista(string &artista) {
	this->artista = artista;
}


/** 
 * @brief	Ḿétodo que retorna o artista do cd
 * @return 	artista do cd.
 */
string Cd::get_artista() {
	return artista;
}


/** 
 * @brief	Ḿétodo que altera o nome do album do cd
 */
void Cd::set_album(string &album) {
	this->album = album;
}


/** 
 * @brief	Ḿétodo que retorna o nome album do cd
 * @return 	album do cd.
 */
string Cd::get_album() {
	return album;
}


/** 
 * @brief	Sobrecarga do operador == para para verificar se
 *			existe código de barras na lista de produtos igual a 
 *			do produto passado.
 */
bool Cd::operator== (Cd& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}
	
}