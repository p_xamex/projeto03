/**
* @file	 	perecivel.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Perecivel.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#include "perecivel.h"

namespace qltLib {
/** 
 * @brief	Ḿétodo que retorna a validade do protudo
 * @return 	validade do produto.
 */
int Perecivel::get_validade(){
	return validade;
}


/** 
 * @brief	Ḿétodo que altera a validade do protudo
 */
void Perecivel::set_validade(int val){
	validade = val;
}


/** 
 * @brief	Ḿétodo que checa se o produto está dentro do prazo de validade.
 * @return 	estado da validade do produto, false para estragado, true para 
 *			bom para consumo.
 */
bool Perecivel::checa_validade(int dia){
	if(validade >= dia){
		return false;
	}else{
		return true;
	}
}
	
}