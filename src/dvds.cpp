/**
* @file	 	dvds.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Dvd.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#include "dvds.h"

namespace qltLib {
/** 
 * @brief	Construtor padrão da classe Dvd
 */
Dvd::Dvd(){
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
	titulo = "Desconhecido";
	genero = "Desconhecido";
	d_total = 0;
}


/** 
 * @brief	Destrutor padrão da classe Dvd
 */
Dvd::~Dvd(){

}


/** 
 * @brief	Construtor parametrizado da classe Dvd
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	t nome do título do dvd.
 * @param 	ge gênero do conteúdo do dvd.
 * @param	dt duração total (em minutos) do dvd.
 */
Dvd::Dvd(int c, string d, float p, int q, string t, string ge, float dt){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_titulo(t);
	set_genero(ge);
	set_dt(dt);
	next = NULL;
	prev = NULL;
	this->lotes++;
	this->quant++;
}


/** 
 * @brief	Ḿétodo que altera o titulo do dvd
 */
void Dvd::set_titulo(string t){
	titulo = t;
}


/** 
 * @brief	Ḿétodo que retorna o titulo do dvd
 * @return 	titulo do dvd.
 */
string Dvd::get_titulo(){
	return titulo;
}


/** 
 * @brief	Ḿétodo que altera o gênero do dvd
 */
void Dvd::set_genero(string ge){
	genero = ge;
}


/** 
 * @brief	Ḿétodo que retorna o gênero do dvd
 * @return 	gênero do dvd.
 */
string Dvd::get_genero(){
	return genero;
}


/** 
 * @brief	Ḿétodo que altera a duração total do dvd
 */
void Dvd::set_dt(float dt){
	d_total = dt;
}


/** 
 * @brief	Ḿétodo que retorna a duração total do dvd
 * @return  a duração total do dvd.
 */
float Dvd::get_dt(){
	return d_total;
}


/** 
 * @brief	Sobrecarga do operador == para para verificar se
 *			existe código de barras na lista de produtos igual a 
 *			do produto passado.
 */
bool Dvd::operator== (Dvd& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}
	
}