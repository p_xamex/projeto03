/**
* @file	 	salgados.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Salgado.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/
#include "salgados.h"

namespace qltLib {
/** 
* @brief	Construtor padrão da classe Salgado
*/
Salgado::Salgado(){
	sodio = 0;
	gluten = false;
	lactose = false;
	validade = 999999;
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
}


/** 
* @brief	Destrutor padrão da classe Salgado
*/
Salgado::~Salgado(){
}


/** 
* @brief	Construtor parametrizado da classe Salgado
* @param	c código de barras do produto.
* @param	d descrição do produto.
* @param	p preço do produto.
* @param	q quantidade de unidades do produto.
* @param	sod quantidade de sodio no salgado.
* @param	g booleana para determinar se contém gluten.
* @param	lac booleana para determinar se contém lactose.
* @param	val data de validade do produto.
*/
Salgado::Salgado(int c, string d, float p, int q, float sod, bool g, bool lac, int val){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_sodio(sod);
	set_gluten(g);
	set_lactose(lac);
	set_validade(val);
	this->lotes++;
	this->quant++;
}


/**\defgroup Gets_e_Sets_Salgado
* @brief 	Métodos de get e set da classe Salgado
* @{
*/
void Salgado::set_sodio(float s){
	sodio = s;
}

float Salgado::get_sodio(){
	return sodio;
}

void Salgado::set_gluten(int g){
	if(g == 1){
		gluten = true;
	}else{
		gluten = false;
	}
}

bool Salgado::get_gluten(){

	return gluten;
}

void Salgado::set_lactose(int l){
	if(l == 1){
		lactose = true;
	}else{
		lactose = false;
	}
}

bool Salgado::get_lactose(){

	return lactose;
}
/**
* @}
*/


/** 
* @brief	Sobrecarga do operador == para para verificar se
*			existe código de barras na lista de produtos igual a 
*			do produto passado.
*/
bool Salgado::operator== (Salgado& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}
	
}