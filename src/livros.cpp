/**
* @file	 	livros.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Livro.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#include "livros.h"

namespace qltLib {
/** 
 * @brief	Construtor padrão da classe Livro
 */
Livro::Livro(){
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
	titulo = "Desconhecido";
	autor = "Desconhecido";
	editora = "Desconhecida";
	ano  = 0;
}


/** 
 * @brief	Destrutor padrão da classe Livro
 */
Livro::~Livro(){

}


/** 
 * @brief	Construtor parametrizado da classe Livro
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	titulo nome do título do livro.
 * @param 	autor nome do autor do livro.
 * @param	edit editora do livro.
 * @param	ano ano de publicação do livro.
 */
Livro::Livro(int c, string d, float p, int q, string titulo, string autor, string editora, int ano){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_titulo(titulo);
	set_autor(autor);
	set_editora(editora);
	set_ano(ano);
	this->lotes++;
	this->quant++;
}


/** 
 * @brief	Ḿétodo que altera o titulo do Livro
 */
void Livro::set_titulo(string titulo) {
	this->titulo = titulo;
}


/** 
 * @brief	Ḿétodo que retorna o titulo do livro
 * @return 	titulo do livro.
 */
string Livro::get_titulo() {
	return titulo;
}


/** 
 * @brief	Ḿétodo que altera o autor do Livro
 */
void Livro::set_autor(string autor) {
	this->autor = autor;
}


/** 
 * @brief	Ḿétodo que retorna o autor do livro
 * @return 	autor do livro.
 */
string Livro::get_autor() {
	return autor;
}


/** 
 * @brief	Ḿétodo que altera a editora do Livro
 */
void Livro::set_editora(string editora){
	this->editora = editora;
}


/** 
 * @brief	Ḿétodo que retorna a editora do livro
 * @return 	editora do livro.
 */
string Livro::get_editora(){
	return editora;
}


/** 
 * @brief	Ḿétodo que altera o ano do Livro
 */
void Livro::set_ano(int ano) {
	this->ano = ano;
}


/** 
 * @brief	Ḿétodo que retorna o ano do livro
 * @return 	ano do livro.
 */
int Livro::get_ano() {
	return ano;
}


/** 
 * @brief	Sobrecarga do operador == para para verificar se
 *			existe código de barras na lista de produtos igual a 
 *			do produto passado.
 */
bool Livro::operator== (Livro& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}
	
}