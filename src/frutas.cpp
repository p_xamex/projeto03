/**
* @file	 	frutas.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Fruta.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/
#include "frutas.h"


namespace qltLib {
	
/** 
* @brief	Construtor padrão da classe Fruta
*/
Fruta::Fruta(){
	lote = 0;
	data = 0; 

	validade = 999999;
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
}

/** 
* @brief	Construtor parametrizado da classe Fruta
* @param	c código de barras do produto.
* @param	d descrição do produto.
* @param	p preço do produto.
* @param	q quantidade de unidades do produto.
* @param	acu quantidade de acucar no doce.
* @param	g booleana para determinar se contém gluten.
* @param	lac booleana para determinar se contém lactose.
* @param	val data de validade do produto.
*/
Fruta::Fruta(int c, string d, float p, int q, int l, int dt, int val){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_lote(l);
	set_data(dt);
	set_validade(val);
	this->lotes++;
	this->quant++;
}

/** 
* @brief	Destrutor padrão da classe Fruta
*/ 
Fruta::~Fruta(){ 

}

/**\defgroup Gets_e_Sets_Fruta
* @brief 	Métodos de get e set da classe Fruta
* @{
*/
void Fruta::set_lote(int l){
	lote = l;
}

int Fruta::get_lote(){
	return lote;
}

void Fruta::set_data(int dt){
	data = dt;
}

int Fruta::get_data(){
	return data;
}
/**
* @}
*/

/** 
* @brief	Sobrecarga do operador == para para verificar se
*			existe código de barras na lista de produtos igual a 
*			do produto passado.
*/
bool Fruta::operator== (Fruta& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}
}