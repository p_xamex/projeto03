#include "registro.h"
#include "produtos.h"
#include "unistd.h"

namespace qltLib {
const string arrayProdutos[7] = {"Bebidas", "Frutas", "Salgados", "Doces", "Cds", "Dvds", "Livros"};



/**
* @brief Função que checa se um saida existe 
* @brief Retirado do stackoverflow
* @brief http://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c
*/
inline bool Registro::fileExists(const std::string& name) {
    ifstream f(name.c_str());
    bool value = f.good();
    f.close();
    return value;
}

void Registro::abrirRegistro() {
 	saida.open(DIR);
}

void Registro::fecharRegistro() {
    saida.close();
}

void Registro::criarRegistro(Lista &l) {
	saida.open(DIR);
	if (!saida) {
		cerr << "Erro ao criar registro." << endl;
		cerr << "Verifique diretório de execução." << endl;
	}
	atualizarRegistro(l);
	saida.close();
	
}

void Registro::carregarRegistro(Lista &l) {
	string produto;
	entrada >> produto;
	entrada.ignore(std::numeric_limits<streamsize>::max(),'\n');

	Bebida *b = new Bebida;
	lerProduto(l, b, produto, 0);

	
}

Registro::Registro(Lista &l) {
	time_t agora = time(0);
	struct tm *timeStruct = localtime(&agora);
	this->dia = timeStruct->tm_mday;
	this->mes = timeStruct->tm_mon;
	this->ano = timeStruct->tm_year + 1900;

	if (!fileExists(DIR)) {
		cout << "Registro de estoque não encontrado!" << endl;
		sleep(1);
		cout << "Criando novo registro..." << endl;
		sleep(2);
		criarRegistro(l);
	} else {
		entrada.open(DIR);
		if (!entrada) cerr << "Erro ao carregar registro!" << endl;
		//carregarRegistro(l);
		entrada.close();
	}
}


Registro::~Registro() {
	fecharRegistro();
}

void Registro::escreve(Bebida *bebida) {
	saida << bebida->get_teor() << ";" << bebida->get_acucar() << ";" << bebida->get_validade() << endl;
}

void Registro::escreve(Fruta *fruta) {
	saida << fruta->get_lote() << ";" << fruta->get_validade() << endl;
}

void Registro::escreve(Salgado *salgado) {
	saida << salgado->get_sodio() << ";" << salgado->get_gluten() << ";" << salgado->get_lactose() << ";" << salgado->get_validade() << endl;
}

void Registro::escreve(Doce *doce) {
	saida << doce->get_acucar() << ";" << doce->get_gluten() << ";" << doce->get_lactose() << ";" << doce->get_validade() << endl;
}

void Registro::escreve(Livro *livro) {
	saida << livro->get_titulo() << ";" << livro->get_autor() << ";" << livro->get_editora() << ";" << livro->get_ano() << endl;
}

void Registro::escreve(Dvd *dvd) {
	saida << dvd->get_titulo() << ";" << dvd->get_genero() << ";" << dvd->get_dt() << endl;
}

void Registro::escreve(Cd *cd) {
	saida << cd->get_estilo() << ";" << cd->get_artista() << ";" << cd->get_album() << endl;
}

template <typename produtoT>
void Registro::escreverProduto(produtoT *produto, int tipo) {
	if (!produto->quant) {
		saida << "Sem " << arrayProdutos[tipo] << " cadastrados(as)" << endl;
	} else {
		produtoT *sentry = produto->next;
		while (sentry->next != NULL) {
			saida << sentry->get_desc() << ";" << sentry->get_cdb() << ";" << sentry->get_quant() << ";" << sentry->get_preco() << ";";
			escreve(sentry);			
			sentry = sentry->next;
		}
	}
}

void Registro::atualizarRegistro(Lista &l) {
	if (!saida) {
		saida.open(DIR, std::ofstream::trunc);
	}
	for (int i = 0; i < 7; i++) {
		saida << arrayProdutos[i] << ";" << "Descrição;" << "CDB;" << "Quantidade;" << "Preço;";
		switch(i) {
			case 0: saida << "Teor Alcólico;" << "Açúcar por mg;" << "Validade" << endl;
					escreverProduto(l.bh, i);
				break;
			case 1: saida << "Lote;" << "Validade" << endl;
					escreverProduto(l.fh, i);
				break;
			case 2:	saida << "Sodio;" << "Glúten;" << "Lactose;" << "Validade" << endl;
					escreverProduto(l.sh, i);
				break;
			case 3: saida << "Açúcar por mg;" << "Glúten;" << "Lactose;" << "Validade" << endl;
					escreverProduto(l.dh, i);
				break;
			case 4: saida << "Estilo;" << "Artista;" << "Albúm" << endl;
					escreverProduto(l.ch, i);
				break;
			case 5: saida << "Título;" << "Gênero;" << "Lançamento" << endl;
					escreverProduto(l.dvh, i);
				break;
			case 6: saida << "Título" << "Autor" << "Editora" << "Lançamento" << endl;
					escreverProduto(l.lh, i);
				break;
		}
		
	}
	saida << "Data de Criação;" << dia << ";" << mes << ";" << ano << endl;
	saida.close();

}


void Registro::ler(Bebida *bebida, Lista &l) {
	float teor, acucar;
	int validade;

	entrada >> teor;
	entrada.ignore();
	entrada >> acucar;
	entrada.ignore();
	entrada >> validade;

	cout << teor << endl;
	cout << acucar << endl;
	cout << validade << endl;

	l.inserir_inicio_b(bebida->get_cdb(), bebida->get_desc(), 
		bebida->get_preco(), bebida->get_quant(), teor, acucar, validade);
}

void Registro::ler(Fruta *fruta, Lista &l) {
	int lote, validade;
	entrada >> lote;
	entrada.ignore();
	entrada >> validade;
}

void Registro::ler(Salgado *salgado, Lista &l) {
	float sodio;
	int validade;
	bool gluten, lactose;

	entrada >> sodio;
	entrada.ignore(); 
	entrada >> gluten;
	entrada.ignore();
	entrada >> lactose;
	entrada.ignore();
	entrada >> validade;
}

void Registro::ler(Doce *doce, Lista &l) {
	float acucar;
	int validade;
	bool gluten, lactose;

	entrada >> acucar;
	entrada.ignore();
	entrada >> gluten;
	entrada.ignore(); 
	entrada >> lactose; 
	entrada.ignore();
	entrada >> validade;
}

void Registro::ler(Livro *livro, Lista &l) {
	string titulo, autor, editora;
	int ano;

	entrada >> titulo;
	entrada.ignore(); 
	entrada >> autor;
	entrada.ignore();
	entrada >> editora;
	entrada.ignore();
	entrada >> ano;
}

void Registro::ler(Dvd *dvd, Lista &l) {
	string titulo, genero;
	int lancamento;

	entrada >> titulo;
	entrada.ignore();
	entrada >> genero;
	entrada.ignore();
	entrada >> lancamento;
}

void Registro::ler(Cd *cd, Lista &l) {
	string estilo, artista, album;

	entrada >> estilo;
	entrada.ignore();
	entrada >> artista;
	entrada.ignore();
	entrada >> album;
}

template <typename produtoT>
void Registro::lerProduto(Lista &l, produtoT *produtoTipo, string produto, int tipo) {
	string desc;
	float preco;
	int quant, cdb;

		getline(entrada, produto, ';');
		getline(entrada, desc, ';');
		entrada >> cdb;
		entrada.ignore();
		entrada >> quant;
		entrada.ignore();
		entrada >> preco;
		entrada.ignore();


		produtoTipo->set_desc(desc);
		produtoTipo->set_cdb(cdb);
		produtoTipo->set_quant(quant);
		produtoTipo->set_preco(preco);
		
		cout << produto << endl;
		cout << desc << endl;
		cout << cdb << endl;
		cout << quant << endl;

		ler(produtoTipo, l);

		
		entrada.ignore(std::numeric_limits<streamsize>::max(),'\n');
		getline(entrada, produto, ';');
		cout << produto << endl;



}

	
}
