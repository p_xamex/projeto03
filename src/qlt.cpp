/**
* @file	 	qlt.cpp
* @brief	Arquivo de corpo contendo as implementações da Classe Qlt,
*			responsável por gerenciar e alterar a lista de produtos e compras.
* @details 	A classe lista contém um iterador para cada map como membro, usado para 
*			poupar linhas de código e declarações desnecessárias.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/


#include "qlt.h"

namespace qltLib {

/** 
* @brief	Método que lista todas os produtos cadastrados do estoque.
*/
void Qlt::listar()
{

	cout << "\n\n-----Setor de Bebida-----\n";
	em_comum_listar(beb);
	cout << "\n\n-----Setor de Frutas-----\n";
	em_comum_listar(frut);
	cout << "\n\n-----Setor de Doces-----\n";
	em_comum_listar(doce);
	cout << "\n\n-----Setor de Salgados-----\n";
	em_comum_listar(salg);
	cout << "\n\n-----Setor de Cds-----\n";
	em_comum_listar(cd);
	cout << "\n\n-----Setor de Dvds-----\n";
	em_comum_listar(dvd);
	cout << "\n\n-----Setor de Livros-----\n";
	em_comum_listar(livr);

}


/** 
 * @brief	Método responsável por checar se um produto com o mesmo
 *			código de barras já foi registrado.
 * @param	cdb código de barras do produto.
 */
int Qlt::checa_cdb(int cdb)
{
	if(beb.count(cdb) > 0 || frut.count(cdb) > 0 || doce.count(cdb) > 0 || salg.count(cdb) > 0 || cd.count(cdb) > 0 || dvd.count(cdb) > 0 || livr.count(cdb) > 0)
	{
		cout << "\nProduto com mesmo código de barras já cadastrado\n";
		return 1;

	}else return 0;
}


/** 
 * @brief	Método que consulta as informções de um determinado
 *			produto ou setor de produtos.
 * @param	esc escolha do usuário (setor ou produto).
 */
void Qlt::consultar(int esc){
	int c = 0;
	cout << "\n\nEm qual setor de produtos deseja realizar a consulta: ";
	c = escolha_setor();
	if(c == 1){
		itbeb = beb.begin();
		if(esc == 2){
			while(itbeb != beb.end()){
				em_comum_produto(itbeb);
		 		itbeb++;
			}

		}else if(em_comum_consultar(beb, itbeb) > 0){
			cout << "Teor alcólico: " << itbeb->second.get_teor() << "\nQuantidade de açúcar por ml: " << itbeb->second.get_acucar();
			cout << "\nData de validade: " << itbeb->second.get_validade() << endl;

		}
	
	}else if(c == 2){
		itfrut = frut.begin();
		if(esc == 2){
			while(itfrut != frut.end()){
				em_comum_produto(itfrut);
		 		itfrut++;
			}

		}else if(em_comum_consultar(frut, itfrut) > 0){
			cout << "Número do lote: " << itfrut->second.get_lote() << "\nData do lote: " << itfrut->second.get_data();
			cout << "\nData de validade: " << itfrut->second.get_validade() << endl;
			}
		
	}else if(c == 3){
		itsalg = salg.begin();
		if(esc == 2){
			while(itsalg != salg.end()){
				em_comum_produto(itsalg);
				cout << "Quantidade de Sódio por miligrama: " << itsalg->second.get_sodio() << "\nGlúten: ";
				gluten_lactose_data(itsalg);
				itsalg++;
			}

		}else if(em_comum_consultar(salg, itsalg) > 0){
			cout << "Quantidade de Sódio por miligrama: " << itsalg->second.get_sodio() << "\nGlúten: ";
			gluten_lactose_data(itsalg);
		}

	}else if(c == 4){
		itdoce = doce.begin();
		if(esc == 2){
			while(itdoce != doce.end()){
				em_comum_produto(itdoce);
				cout << "Quantidade de Açúcar por miligrama: " << itdoce->second.get_acucar() << "\nGlúten: ";
				gluten_lactose_data(itdoce);
				itdoce++;
			}

		}else if(em_comum_consultar(doce, itdoce) > 0){
			cout << "Quantidade de Açúcar por miligrama: " << itdoce->second.get_acucar() << "\nGlúten: ";
			gluten_lactose_data(itdoce);
		}

	}else if(c == 5){
		itcd = cd.begin();
		if(esc == 2){
			while(itcd != cd.end()){
				em_comum_produto(itcd);
				cout << "Estilo: " << itcd->second.get_estilo() << "\nArtista: " << itcd->second.get_artista();
				cout << "\nAlbum: " << itcd->second.get_album() << endl;
				itcd++;
			}
		}else if(em_comum_consultar(cd, itcd) > 0){
			cout << "Estilo: " << itcd->second.get_estilo() << "\nArtista: " << itcd->second.get_artista();
			cout << "\nAlbum: " << itcd->second.get_album() << endl;
		}

	}else if(c == 6){
		itdvd = dvd.begin();
		if(esc == 2){
			while(itdvd != dvd.end()){
				em_comum_produto(itdvd);
				cout << "Título: " << itdvd->second.get_titulo() << "\nGênero: " << itdvd->second.get_genero();
				cout << "\nDuração total: " << itdvd->second.get_dt() << endl;		
				itdvd++;
			}
		}else if(em_comum_consultar(dvd, itdvd) > 0){
			cout << "\nTítulo: " << itdvd->second.get_titulo() << "\nGênero: " << itdvd->second.get_genero();
			cout << "\nDuração total: " << itdvd->second.get_dt() << endl;		
		}

	}else if(c == 7){
		itlivr = livr.begin();
		if(esc == 2){
			while(itlivr != livr.end()){
				em_comum_produto(itlivr);
				cout << "Título: " << itlivr->second.get_titulo() << "\nAutor: " << itlivr->second.get_autor();
				cout << "\nEditora: " << itlivr->second.get_editora() << "\nAno: " << itlivr->second.get_ano() << endl;		
				itlivr++;
			}

		}else if(em_comum_consultar(livr, itlivr) > 0){
		cout << "Título: " << itlivr->second.get_titulo() << "\nAutor: " << itlivr->second.get_autor();
		cout << "\nEditora: " << itlivr->second.get_editora() << "\nAno: " << itlivr->second.get_ano() << endl;		
		}
	}
}


/** 
 * @brief	Método que remove um produto específico.
 */
void Qlt::remover(){
	int c = 0;
	cout << "\n\nEm qual setor de produtos deseja realizar a remoção: ";
	c = escolha_setor();

	if(c == 1){
		itbeb = beb.begin();
		em_comum_remover(beb, itbeb);

	}else if(c == 2){
		itfrut = frut.begin();
		em_comum_remover(frut, itfrut);

	}else if(c == 3){
		itsalg = salg.begin();
		em_comum_remover(salg, itsalg);

	}else if(c == 4){
		itdoce = doce.begin();
		em_comum_remover(doce, itdoce);

	}else if(c == 5){
		itcd = cd.begin();
		em_comum_remover(cd, itcd);

	}else if(c == 6){
		itdvd = dvd.begin();
		em_comum_remover(dvd, itdvd);

	}else if(c == 7){
		itlivr = livr.begin();
		em_comum_remover(livr, itlivr);
	}
}



/** 
* @brief	Método que altera um produto específico.
*/
void Qlt::alterar(){
	float codigo;
	codigo = 0;

	int c;
	c = 0;

	cout << "\n\nEm qual setor de produtos deseja realizar a alteração: ";
	c = escolha_setor();

	cout << "\n\n------Alteração------\n";
	if (c == 1){
		itbeb = beb.begin();
		Bebida nova;
		if(em_comum_consultar(beb, itbeb) > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (checa_cdb(codigo) == 0){
				nova.set_cdb(codigo);
				em_comum_alterar(nova);

				cout << "\nTeor alcólico: ";
				float teor;
				invalida(teor);
				nova.set_teor(teor);

				cout << "\nQuantidade de açúcar por miligrama: ";
				float acucar;
				invalida(acucar);
				nova.set_acucar(acucar);

				cout << "\nData de validade do produto: ";
				int validade;
				invalida(validade);
				nova.set_validade(validade);

				beb.erase(itbeb->first);
				beb[codigo] = nova;
			}
		}

	}else if(c == 2){
		itfrut = frut.begin();
		Fruta nova;
		if(em_comum_consultar(frut, itfrut) > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (checa_cdb(codigo) == 0){
				nova.set_cdb(codigo);
				em_comum_alterar(nova);
				cout << "\nNúmero do lote: ";
				int lote;
				invalida(lote);
				nova.set_lote(lote);

				cout << "\nData do lote: ";
				int data;
				invalida(data);
				nova.set_data(data);

				cout << "\nData de validade do produto: ";
				int validade;
				invalida(validade);
				nova.set_validade(validade);

				frut.erase(itfrut->first);
				frut[codigo] = nova;
			}
		}

	}else if(c == 3){
		itsalg = salg.begin();
		Salgado novo;
		if(em_comum_consultar(salg, itsalg) > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (checa_cdb(codigo) == false){
				novo.set_cdb(codigo);
				em_comum_alterar(novo);

				cout << "\nQuantidade de sódio por miligrama: ";
				float so;
				invalida(so);
				novo.set_sodio(so);

				cout << "\nContém glúten: ";
				int cg;
				alergicos(cg);
				novo.set_gluten(cg);

				cout << "\nContém lactose: ";
				int cs;
				alergicos(cs);
				novo.set_lactose(cs);

				cout << "\nData de validade do produto: ";
				int validade;
				invalida(validade);
				novo.set_validade(validade);

				salg.erase(itsalg->first);
				salg[codigo] = novo;
			}

		}

	}else if(c == 4){
		itdoce = doce.begin();
		Doce novo;
		if(em_comum_consultar(doce, itdoce) > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (checa_cdb(codigo) == 0){
				novo.set_cdb(codigo);
				em_comum_alterar(novo);

				cout << "\nQuantidade de açúcar por miligrama: ";
				float so;
				invalida(so);
				novo.set_acucar(so);

				cout << "\nContém glúten: ";
				int cg;
				alergicos(cg);
				novo.set_gluten(cg);

				cout << "\nContém lactose: ";
				int cs;
				alergicos(cs);
				novo.set_lactose(cs);

				cout << "\nData de validade do produto: ";
				int validade;
				invalida(validade);

				novo.set_validade(validade);

				doce.erase(itdoce->first);
				doce[codigo] = novo;
			}

		}

	}else if(c == 5){
		itcd = cd.begin();
		Cd novo;
		if(em_comum_consultar(cd, itcd) > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (checa_cdb(codigo) == 0){
				novo.set_cdb(codigo);
				em_comum_alterar(novo);
				
				cout << "\nEstilo: ";
				cin.ignore();
				string estilo;
		    	getline(cin, estilo, '\n');
				novo.set_estilo(estilo);

				cout << "\nArtista: ";
				string art;
		    	getline(cin, art, '\n');
				novo.set_artista(art);

				cout << "\nAlbum: ";
				string album;
		    	getline(cin, album, '\n');
				novo.set_album(album);

				cd.erase(itcd->first);
				cd[codigo] = novo;
			}
		} 

	}else if(c == 6){
		itdvd = dvd.begin();
		Dvd novo;
		if(em_comum_consultar(dvd, itdvd) > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (checa_cdb(codigo) == 0){
				novo.set_cdb(codigo);
				em_comum_alterar(novo);

				cout << "\nTítulo: ";
				string t;
				cin.ignore();
				getline(cin, t, '\n');
				novo.set_titulo(t);

				cout << "\nGênero: ";
				string ge;
				getline(cin, ge, '\n');
				novo.set_genero(ge);

				cout << "\nDuração total: ";
				float dt;
				invalida(dt);
				novo.set_dt(dt);

				dvd.erase(itdvd->first);
				dvd[codigo] = novo;
			}

		}

	}else if(c == 7){
		itlivr = livr.begin();
		Livro novo;
		if(em_comum_consultar(livr, itlivr) > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (checa_cdb(codigo) == 0){
				novo.set_cdb(codigo);
				em_comum_alterar(novo);

				cout << "\nTítulo: ";
				string t;
				cin.ignore();
				getline(cin, t, '\n');
				novo.set_titulo(t);

				cout << "\nAutor: ";
				string a;
				getline(cin, a, '\n');
				novo.set_autor(a);

				cout << "\nEditora: ";
				string edit;
				getline(cin, edit, '\n');
				novo.set_editora(edit);

				cout << "\nAno: ";
				int ano;
				invalida(ano);
				novo.set_ano(ano);

				livr.erase(itlivr->first);
				livr[codigo] = novo;
			}
		}
	}
}




/** 
 * @brief	Método que inicia a tarefa de compra, chamando as
 *			funções e metódos que auxiliam a atividade.
 */
void Qlt::compra(){
	float total = Compra::total; // por algum motivo não da pra alterar o valor de Compra::total diretamente nessa parte do programa, por isso foi usada uma variavel auxiliar
	int carrinho = Compra::carrinho;
	int esc;
	cout << "\ninforme em qual setor deseja comprar\n";
	esc = escolha_setor();
	iterator_to_begin();
	if(esc == 1){
		if(em_comum_consultar(beb, itbeb) > 0){
			if(itbeb->second.get_quant() != 0) em_comum_compra(itbeb, beb, comp, total, carrinho);
		}

	}else if(esc == 2){
		if(em_comum_consultar(frut, itfrut) > 0){
			if(itfrut->second.get_quant() != 0) em_comum_compra(itfrut, frut, comp, total, carrinho);
		}

	}else if(esc == 3){
		if(em_comum_consultar(salg, itsalg) > 0){
			if(itsalg->second.get_quant() != 0) em_comum_compra(itsalg, salg, comp, total, carrinho);
		}

 
	}else if(esc == 4){
		if(em_comum_consultar(doce, itdoce) > 0){
			if(itdoce->second.get_quant() != 0) em_comum_compra(itdoce, doce, comp, total, carrinho);
		}


	}else if(esc == 5){
		if(em_comum_consultar(cd, itcd) > 0){
			if(itcd->second.get_quant() != 0) em_comum_compra(itcd, cd, comp, total, carrinho);
		}


	}else if(esc == 6){
		if(em_comum_consultar(dvd, itdvd) > 0){
			if(itdvd->second.get_quant() != 0) em_comum_compra(itdvd, dvd, comp, total, carrinho);
		}


	}else if(esc == 7){
		if(em_comum_consultar(livr, itlivr) > 0){
			if(itlivr->second.get_quant() != 0) em_comum_compra(itlivr, livr, comp, total, carrinho);
		}


	}

	Compra::total=total; //unica maneira encontrada de incrementar o contador de Compras
	Compra::carrinho=carrinho;//unica maneira encontrada de incrementar o contador de Compras}
}

/** 
 * @brief	Método que posiciona todos os iteradores no inicio de suas
 *			respectivas listas, necessaria quando o iterador é alterado por
 *			outro método.
 */
void Qlt::iterator_to_begin()
{
	itbeb = beb.begin();
	itfrut = frut.begin();
	itsalg = salg.begin();
	itdoce = doce.begin();
	itcd = cd.begin();
	itdvd = dvd.begin();
	itlivr = livr.begin();
	itcomp = comp.begin();

}

/** 
* @brief	Método que lista todas as compras feitas.
*/
void Qlt::lista_compras()
{
	itcomp = comp.begin();
	for(; itcomp != comp.end(); ++itcomp){
		cout << itcomp->second.get_desc() << endl;
		cout << ' ' << itcomp->second.get_quant() << " UN X "<< itcomp->second.get_preco();
		cout << "\t\t\t" << itcomp->second.get_quant() * itcomp->second.get_preco() << endl << endl;  		
			
	}
	cout << endl;
}
	
}
