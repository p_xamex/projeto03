/**
* @file	 	compras.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Compra.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#include "compras.h"


namespace qltLib {
/** 
 * @brief	Construtor padrão da classe Compra
 */
Compra::Compra(){
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
	total = 0;
	carrinho = 0;
}


/** 
 * @brief	Destrutor padrão da classe Compra
 */
Compra::~Compra(){

}


/** 
 * @brief	Construtor parametrizado da classe Compra
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 */
Compra::Compra(int c, string d, float p, int q){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	next = NULL;
	prev = NULL;
	carrinho += q;
}


/** 
 * @brief	Sobrecarga do operador + para somar a quantidade 
 *			de unidades de dois produtos.
 * @details	soma a quantidade de um produto à do que chamou o método.
 */
void Compra::operator+ (Compra& c){
	quant += c.quant;
}


/** 
 * @brief	Sobrecarga do operador == para para verificar se
 *			existe código de barras na lista de produtos igual a 
 *			do produto passado.
 */
bool Compra::operator== (Compra& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}
	
}