/**
* @file	 	doces.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Doce.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/
#include "doces.h"

namespace qltLib {
/** 
* @brief	Construtor padrão da classe Doce
*/
Doce::Doce(){
	acucar = 0;
	gluten = false;
	lactose = false;
	validade = 999999;
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
}


/** 
* @brief	Destrutor padrão da classe Doce
*/
Doce::~Doce(){
}


/** 
* @brief	Construtor parametrizado da classe Doce
* @param	c código de barras do produto.
* @param	d descrição do produto.
* @param	p preço do produto.
* @param	q quantidade de unidades do produto.
* @param	acu quantidade de acucar no doce.
* @param	g booleana para determinar se contém gluten.
* @param	lac booleana para determinar se contém lactose.
* @param	val data de validade do produto.
*/
Doce::Doce(int c, string d, float p, int q, float acu, bool g, bool lac, int val){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_acucar(acu);
	set_gluten(g);
	set_lactose(lac);
	set_validade(val);
	this->lotes++;
	this->quant++;
}


/**\defgroup Gets_e_Sets_Doce
* @brief 	Métodos de get e set da classe Doce
* @{
*/
void Doce::set_acucar(float s){
	acucar = s;
}

float Doce::get_acucar(){
	return acucar;
}

void Doce::set_gluten(int g){
	if(g == 1){
		gluten = true;
	}else{
		gluten = false;
	}
}

bool Doce::get_gluten(){

	return gluten;
}

void Doce::set_lactose(int l){
	if(l == 1){
		lactose = true;
	}else{
		lactose = false;
	}
}

bool Doce::get_lactose(){

	return lactose;
}
/**
* @}
*/

/** 
* @brief	Sobrecarga do operador == para para verificar se
*			existe código de barras na lista de produtos igual a 
*			do produto passado.
*/
bool Doce::operator== (Doce& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}
	
}