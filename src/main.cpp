/**
* @file	 	main.cpp
* @brief	Programa que realiza cadastro e venda de produtos para a loja
*			de conveniência QLeveTudo.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/

#include <iostream> 
#include "qltLib.h" 

using namespace qltLib;
//Inicialização das variáveis estáticas usadas para a contagem
// da quantide total de objetos das respectivas classes. 
int Produto::lotes = 0;
int Bebida::quant = 0;
int Cd::quant = 0;
int Doce::quant = 0;
int Dvd::quant = 0;
int Fruta::quant = 0;
int Livro::quant = 0;
int Salgado::quant = 0;
float Compra::total = 0; // valor total das compras feitas
int Compra::carrinho = 0;

int main() {
	//Qlt produto;

	//acessar_lista_p(produto);

	return 0;
}
