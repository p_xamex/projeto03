/**
* @file	 	menu.cpp
* @brief	Arquivo de corpo contendo as implementações das funções do menu,
*			responsável por gerenciar o menu de ações sobre a classe Lista.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	27/06/2017
* @date		29/06/2017
*/
  

#include "menu.h"

namespace qltLib {
/** 
* @brief	Função resposável por receber informações do usuário e
*			chamar o metódo cadastrar correspondente passando essa informações.
* @param	prod lista principal do programa.
*/
void cadastrar(Qlt &prod){
	ClearScreen();

	cout << "Qual tipo de produto deseja cadastrar:\n";
	cout << "\n1 - Bebida";
	cout << "\n2 - Fruta";
	cout << "\n3 - Salgado";
	cout << "\n4 - Doce";
	cout << "\n5 - Cd";
	cout << "\n6 - Dvd";
	cout << "\n7 - Livro\n\n";


	int escolha;
	escolha_menu(escolha, 1, 7);

	float codigo = 0;
	do{
		cout << "\nCódigo de barras (6 dígitos): ";
		invalida(codigo);
	}while(prod.checa_cdb(codigo) > 0);
	cout << "\nDescrição: ";
	string desc;
	cin.ignore();
    getline(cin, desc, '\n');

	cout << "\nPreço: ";
	float preco;
	invalida(preco);

	cout << "\nQuantidade: ";
	int qnt;
	invalida(qnt);

	if(escolha == 1){
		cout << "\nTeor alcólico: ";
		float teor;
		invalida(teor);

		cout << "\nQuantidade de açúcar por miligrama: ";
		float acucar;
		invalida(acucar);

		cout << "\nData de validade do produto: ";
		int validade;
		invalida(validade);

		Bebida novo(codigo, desc, preco, qnt, teor, acucar, validade);

		prod.beb[novo.get_cdb()] = novo;
	
	}else if(escolha == 2){
		cout << "\nNúmero do lote: ";
		int lote;
		invalida(lote);

		cout << "\nData do lote: ";
		int data;
		invalida(data);

		cout << "\nData de validade do produto: ";
		int validade;
		invalida(validade);

		Fruta novo(codigo, desc, preco, qnt, lote, data, validade);
		prod.frut[novo.get_cdb()] = novo;
	
	}else if(escolha == 3){
		cout << "\nQuantidade de Sódio por miligrama: ";
		float so;
		invalida(so);

		cout << "\nContém glúten: ";
		int cg;
		bool g;
		g = alergicos(cg);

		cout << "\nContém lactose: ";
		int cs;
		bool s;
		s = alergicos(cs);

		cout << "\nData de validade do produto: ";
		int validade;
		invalida(validade);

		Salgado novo(codigo, desc, preco, qnt, so, g, s, validade);
		prod.salg[novo.get_cdb()] = novo;
	}else if(escolha == 4){

		cout << "\nQuantidade de açúcar por miligrama: ";
		float acu;
		invalida(acu);

		cout << "\nContém glúten: ";
		int cg;
		bool g;
		g = alergicos(cg);

		cout << "\nContém lactose: ";
		int cl;
		bool lac;
		lac = alergicos(cl);


		cout << "\nData de validade do produto: ";
		int val;
		invalida(val);

		Doce novo(codigo, desc, preco, qnt, acu, g, lac, val);
		prod.doce[novo.get_cdb()] = novo;

	}else if(escolha == 5){
		cout << "\nEstilo: ";
		string estilo;
		cin.ignore();
    	getline(cin, estilo, '\n');

		cout << "\nArtista: ";
		string art;
    	getline(cin, art, '\n');

		cout << "\nAlbum: ";
		string album;
    	getline(cin, album, '\n');

		Cd novo(codigo, desc, preco, qnt, estilo, art, album);
		prod.cd[novo.get_cdb()] = novo;
	}else if(escolha == 6){
		cout << "\nTítulo: ";
		string t;
		cin.ignore();
		getline(cin, t, '\n');

		cout << "\nGênero: ";
		string ge;
		getline(cin, ge, '\n');

		cout << "\nDuração total: ";
		float dt;
		invalida(dt);

		Dvd novo(codigo, desc, preco, qnt, t, ge, dt);
		prod.dvd[novo.get_cdb()] = novo;

	}else if(escolha == 7){

			cout << "\nTítulo: ";
			string t;
			cin.ignore();
			getline(cin, t, '\n');
			
			cout << "\nAutor: ";
			string a;
			getline(cin, a, '\n');

			cout << "\nEditora: ";
			string edit;
			getline(cin, edit, '\n');
			
			cout << "\nAno: ";
			int ano;
			invalida(ano);
			
			Livro novo(codigo, desc, preco, qnt, t, a, edit, ano);
			prod.livr[novo.get_cdb()] = novo;
	}
	ClearScreen();
}
 

/** 
* @brief	Função que realiza consulta em um setor ou produto específico
* @param	l lista principal do programa.
* @param 	escolha ação escolhida no menu principal.
*/
void acessar_produto(Qlt &l, int escolha){
	int c;
	cout << "\nConsultar um produto específico ou um setor de produtos?";
	cout << "\n1 - Produto";
	cout << "\n2 - Setor\n\n";
	escolha_menu(c, 1, 2);

	l.consultar(c);
}


/** 
* @brief	Função que controla as ações sobre a lista de compras
*			da classe Lista.
* @param	l lista principal do programa.
*/
void menu_compras(Qlt &l){
	ClearScreen();
	int pare;
	pare = 1;
	while(pare == 1){
		cout <<"\n----------Menu de Compras----------\n";
		cout <<"\n1 - Realizar Nova compra";
		cout <<"\n2 - Acessar carrinho de compras";
		cout <<"\n3 - Voltar ao Gerenciador de cadastro\n\n";
		cout << "---------------------------------------";
		cout << "\nValor total das compras feitas: " << Compra::total;
		cout << "\nQuantidade de produtos comprados: " << Compra::carrinho;
		cout << "\n---------------------------------------\n\n";
		int esc;
		escolha_menu(esc, 1, 3);

		ClearScreen();

		if(esc == 1){
			l.compra();

		}else if(esc == 2){
			ClearScreen();
			cout << "\n=============Nota fiscal=============\n\n";
			l.lista_compras();
			cout << "\n=====================================";
			cout << "\n\nVALOR TOTAL R$: " << Compra::total;
			cout << "\n\nQNT. TOTAL DE ITENS: " << Compra::carrinho;
			cout << "\n=====================================\n\n";

		}else{
			pare = 0;
		}
	}

}



/** 
* @brief	Função que controla o menu principal do programa e chama
*			as operações escolhidas pelo usuário.
* @param	l lista principal do programa.
*/
void acessar_lista_p(Qlt &l){
	int escolha;
	ClearScreen();

	int pare;
	pare = 1;

	while(pare == 1){

		cout << "\n---------Gerenciador de cadastros de produtos---------\n";
		cout << "\n1 - Cadastar novo produto";
		cout << "\n2 - Estoque (N° de produtos: " << Produto::lotes << ')';
		cout << "\n3 - Consultar produto ou setor de produtos";
		cout << "\n4 - Remover produto";
		cout << "\n5 - Alterar produto";
		cout << "\n6 - Menu compras";
		cout << "\n7 - Sair\n\n";


		escolha = 0;
		escolha_menu(escolha, 1, 8);

		ClearScreen();
		switch(escolha){
			case 1:
				cadastrar(l);
				break;

			case 2:
				l.listar();
				break;

			case 3:
				acessar_produto(l, escolha);
				break;

			case 4:
				l.remover();
				break;

			case 5:
				l.alterar();
				break;

			case 6:
				menu_compras(l);
				break;

			case 7:
				pare = 0;
				break;

			default:
				break;
		}
	}
}
	
}
