/**
* @file     util.cpp
* @brief    Arquivo de corpo contendo as implementações de funções que realizam
*           operações utilizadas em várias partes do programa.
* @author   Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author   Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since    27/06/2017
* @date     29/06/2017
*/
#include "util.h"
#include "excecao.h"
using std::cerr;

namespace qltLib {
/** 
* @brief    Função que limpa o terminal do linux usando ANSI escape codes
* @details  para mais detalher, acesse o link deixado como comentário ao lado do comando.
*/
void ClearScreen(){
    cout << "\033[2J\033[1;1H"; // codigo retirado da stackoverflow http://stackoverflow.com/questions/4062045/clearing-terminal-in-linux-with-c-code

}


/** 
* @brief    Função que realiza o tratamento de todas as entradas do usuário
            verificando se são válidas ou não.
* @details  parte da função retirada do link: http://stackoverflow.com/questions/4798936/numeric-limits-was-not-declared-in-this-scope-no-matching-function-for-call-t
* @param    num float a ser testado
*/
void invalida(float &num){
    int pare = 1;
	while (pare == 1) 
    {
        try{
            if(!(cin >> num) || num <= 0) throw Tamanho_invalido();
            else pare = 0;
        }catch (Tamanho_invalido &ex){
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cerr << ex.what() << endl;
            }catch(...) {
             cerr << "\nErro desconhecido\n";
        }
	}
}


/** 
* @brief    Função que realiza o tratamento de todas as entradas do usuário
            verificando se são válidas ou não.
* @details  parte da função retirada do link: http://stackoverflow.com/questions/4798936/numeric-limits-was-not-declared-in-this-scope-no-matching-function-for-call-t
* @param    num int a ser testado
*/
void invalida(int &num){
	int pare = 1;
    while (pare == 1) 
    {
        try{
            if(!(cin >> num) || num <= 0) throw Tamanho_invalido();
            else pare = 0;
        }catch (Tamanho_invalido &ex){
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cerr << ex.what() << endl;
            }catch(...) {
             cerr << "\nErro desconhecido\n";
        }
    }
}


/** 
* @brief    Função que rertona se o produto contém ou não alérgico
            de acordo com a escolha passada.
* @param    a variável que armazena a escolha do usuário.
*/
bool alergicos(int &a){
    cout << "\n1 - sim";
    cout << "\n2 - não\n\n";
    do{
        invalida(a);
    }while(a < 1 || a > 2);

    bool sn;

    if(a == 1){
        sn = true;
    }else{
        sn = false;
    }

    return sn;

}


/** 
* @brief    Função que faz o tratamento da escolha em menus.
* @param    esc recebe um valor digitado pelo usuário.
* @param    min valor mínimo do intervalo de opções do menu.
* @param    max valor máximo do intervalo de opções do menu.
*/
void escolha_menu(int &esc, int min, int max){
    do{
        invalida(esc);
    }while(esc < min || esc > max);
}


/** 
* @brief    Função que imprime a tabela de setores e requisita
            ao usuário uma escolha.
*/
int escolha_setor(){
	int c = 0;
	cout << "\n1 - Bebidas";
	cout << "\n2 - Frutas";
	cout << "\n3 - Salgados";
	cout << "\n4 - Doces"; 
	cout << "\n5 - Cds";
	cout << "\n6 - Dvds";
	cout << "\n7 - Livros\n\n";
    
    escolha_menu(c, 1, 7);

	return c;
}
    
}